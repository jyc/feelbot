package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"os/signal"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"bitbucket.org/jyc/feelbot/irc"
)

var l *log.Logger

// config variables
var help bool
var server string
var channel string
var ringLen int
var dataFile string
var nick string
var maxFeelLen int
var savePeriod int

func init() {
	l = log.New(os.Stdout, "feelbot: ", log.LstdFlags)

	flag.BoolVar(&help, "help", false, "show this help screen")
	flag.StringVar(&server, "server", "irc.tacit.io:6667", "server to connect to")
	flag.StringVar(&channel, "channel", "#jychq", "channel to join")
	flag.IntVar(&ringLen, "ring_len", 10, "number of recent URLs to put in the ring")
	flag.StringVar(&dataFile, "data_file", "data.json", "file to store data in")
	flag.StringVar(&nick, "nick", "feelbot", "nickname to use")
	flag.IntVar(&maxFeelLen, "max_feel_len", 12, "maximum feel length")
	flag.IntVar(&savePeriod, "save_period", 60, "how often to save data (in seconds)")
}

func send(conn net.Conn, message *irc.Message) {
	text := irc.Encode(message)
	l.Print("-> ", text)
	_, err := fmt.Fprint(conn, text+"\r\n")
	if err != nil {
		l.Fatal(err)
	}
}

type Page struct {
	Url    string
	Title  string
	Poster string
	Date   time.Time
	Feels  map[string]map[string]bool
}

func makeMostFeelNicks(pages map[string]Page) map[string]map[string]int {
	nickFeelsMap := make(map[string]map[string]int)
	for _, page := range pages {
		for feel, feelers := range page.Feels {
			if nickFeelsMap[feel] == nil {
				nickFeelsMap[feel] = make(map[string]int)
			}
			nickFeelsMap[feel][page.Poster] += len(feelers)
		}
	}
	return nickFeelsMap
}
func makeMostFeelPages(pages map[string]Page) map[string]map[string]int {
	pageFeelsMap := make(map[string]map[string]int)
	for _, page := range pages {
		for feel, feelers := range page.Feels {
			if pageFeelsMap[feel] == nil {
				pageFeelsMap[feel] = make(map[string]int)
			}
			pageFeelsMap[feel][page.Url] = len(feelers)
		}
	}
	return pageFeelsMap
}
func topFromFeelMap(feelMap map[string]map[string]int, feel string, n int) []string {
	// copy the map over...
	clone := make(map[string]int)
	for i, j := range feelMap[feel] {
		clone[i] = j
	}

	top := make([]string, n)
	for i := 0; i < len(top); i++ {
		mj := ""
		mr := -1
		for j, r := range clone {
			if r > mr || (r == mr && j > mj) {
				mj = j
				mr = r
			}
		}

		delete(clone, mj)
		top[i] = mj
	}

	return top
}

func addMostFeel(feelMap map[string]map[string]int, feel string, entity string, n int) {
	if feelMap[feel] == nil {
		feelMap[feel] = make(map[string]int)
	}

	feelMap[feel][entity] += n

	if x, ok := feelMap[feel][entity]; ok && x == 0 {
		delete(feelMap[feel], entity)
	}
	if len(feelMap[feel]) == 0 {
		delete(feelMap, feel)
	}
}

func rootToGerund(root string) string {
	if root[len(root)-1] == 'i' {
		return root[:len(root)-1] + "ying"
	}
	return root + "ing"
}

func gerundToRoot(gerund string) string {
	if gerund[len(gerund)-4:] == "ying" {
		return gerund[:len(gerund)-4] + "i"
	}
	return gerund[:len(gerund)-3]
}

type FeelCount struct {
	Feel  string
	Count int
}

type ByFeelCount []FeelCount

func (f ByFeelCount) Len() int           { return len(f) }
func (f ByFeelCount) Swap(i, j int)      { f[i], f[j] = f[j], f[i] }
func (f ByFeelCount) Less(i, j int) bool { return f[i].Count > f[j].Count } // reversed because we want desc. order

func main() {
	flag.Parse()

	if help {
		flag.Usage()
		return
	}

	pages := make(map[string]Page)
	pagesLock := new(sync.Mutex)

	b, err := ioutil.ReadFile(dataFile)
	if err != nil {
		l.Print("failed to load data file")
	} else {
		l.Print("loaded data file")
		err := json.Unmarshal(b, &pages)
		if err != nil {
			l.Print("failed to unmarshal data file")
		} else {
			l.Print("unmarshalled data file")
		}
	}

	saveData := func() {
		pagesLock.Lock()
		b, err := json.Marshal(pages)
		pagesLock.Unlock()
		if err != nil {
			l.Print(err)
		} else {
			err := ioutil.WriteFile(dataFile, b, 0644)
			if err != nil {
				l.Print(err)
			}
		}
	}

	go func() {
		for {
			time.Sleep(time.Duration(savePeriod) * time.Second)
			saveData()
		}
	}()

	mostFeelNicks := makeMostFeelNicks(pages)
	mostFeelPages := makeMostFeelPages(pages)

	closing := false

	conn, err := net.Dial("tcp", server)
	if err != nil {
		l.Fatal(err)
	}
	errc := make(chan os.Signal, 1)
	signal.Notify(errc, os.Interrupt)
	go func() {
		for _ = range errc {
			l.Print("received SIGINT, exiting!")
			closing = true
			conn.Close()
		}
	}()

	fmt.Fprintf(conn, "NICK "+nick+"\r\n")
	registered := false

	urlRing := make([]string, ringLen)
	lastUrl := -1

	r := bufio.NewReader(conn)
	for {
		line, err := r.ReadString('\n')
		if closing {
			saveData()

			os.Exit(0)
		}
		if err != nil {
			l.Fatal(err)
		}
		line = line[:len(line)-2] // chop \r\n
		l.Print("<- ", line)

		message := irc.Parse(line)

		pagesLock.Lock()

		switch message.Command {
		case "PING":
			send(conn, &irc.Message{
				Command:  "PONG",
				Trailing: message.Trailing})
			if !registered {
				send(conn, &irc.Message{
					Command:  "USER",
					Params:   []string{nick, "0", "*"},
					Trailing: nick})
				registered = true
			}
		case "422", "376": // end of MOTD
			send(conn, &irc.Message{
				Command: "JOIN",
				Params:  []string{channel}})
		case "PRIVMSG":
			if message.Params[0] != channel {
				sender := getNick(message)
				send(conn, &irc.Message{
					Command:  "PRIVMSG",
					Params:   []string{sender},
					Trailing: fmt.Sprintf("Sorry, %s, I only respond to messages on %s.", sender, channel)})
			}

			// convenience function
			reply := func(format string, a ...interface{}) {
				send(conn, &irc.Message{
					Command:  "PRIVMSG",
					Params:   []string{channel},
					Trailing: fmt.Sprintf(format, a...)})
			}

			message.Trailing = strings.Trim(message.Trailing, " \t")

			words := strings.Split(message.Trailing, " ")
			switch words[0] {
			case "!feels":
				feels := make([]FeelCount, len(mostFeelPages))

				i := 0
				for feel, pages := range mostFeelPages {
					count := 0
					for _, n := range pages {
						count += n
					}
					feels[i] = FeelCount{Feel: feel, Count: count}
					i++
				}

				sort.Sort(ByFeelCount(feels))

				var sb bytes.Buffer

				sb.WriteString(getNick(message) + ", a lot of people are ")

				for i := 0; i < 10 && i < len(feels); i++ {
					if i == len(feels)-1 || i == 9 {
						sb.WriteString(fmt.Sprintf("and %sed (%d).", feels[i].Feel, feels[i].Count))
					} else {
						sb.WriteString(fmt.Sprintf("%sed (%d), ", feels[i].Feel, feels[i].Count))
					}
				}

				reply(sb.String())
			case "!most":
				if len(words) != 2 {
					break
				}
				if words[1][len(words[1])-3:] != "ing" {
					reply("Feelings with adjectives that don't end with 'ing' aren't true feelings, %s.", getNick(message))
					break
				}
				feel := strings.ToLower(gerundToRoot(words[1]))
				if _, ok := mostFeelPages[feel]; !ok {
					reply("'%sness' doesn't exist, %s.", words[1], getNick(message))
					break
				}
				mostUrl := topFromFeelMap(mostFeelPages, feel, 1)[0]
				mostNick := topFromFeelMap(mostFeelNicks, feel, 1)[0]
				reply("'%s' (%s) is pretty much the most %s page (%d), and %s the most %s person (%d).", pages[mostUrl].Title, mostUrl, words[1], mostFeelPages[feel][mostUrl], mostNick, words[1], mostFeelNicks[feel][mostNick])
			case "!lookup":
				if len(words) != 2 {
					break
				}

				var url string
				id, err := strconv.Atoi(words[1])
				if err != nil {
					url, err = normalizeUrl(words[1])
					if err != nil {
						break
					}
				} else {
					url = urlRing[id]
				}

				if _, ok := pages[url]; ok {
					page := pages[url]
					var feelDesc string
					if len(page.Feels) == 0 {
						feelDesc = "doesn't make people feel anything"
					} else {
						feels := make([]string, len(page.Feels))
						i := 0
						for feel, feelers := range page.Feels {
							if i == len(page.Feels)-1 && len(page.Feels) > 1 {
								feel = "and " + feel
							}
							feels[i] = fmt.Sprintf("%sed (%d)", feel, len(feelers))
							i++
						}
						feelDesc = "makes people feel " + strings.Join(feels, ", ")
					}
					reply(fmt.Sprintf("%s, '%s' (%s) was posted on %s by %s and %s.", getNick(message), page.Title, url, page.Date.Format(time.UnixDate), page.Poster, feelDesc))
				} else {
					reply("%s, '%s' was not previously posted.", getNick(message), url)
				}
			case "!feel":
				if len(words) < 2 || lastUrl == -1 {
					break
				}

				fp := 1
				id, err := strconv.Atoi(words[1])
				if err != nil {
					id = lastUrl
				} else {
					fp++
				}

				if urlRing[id] == "" {
					break
				}
				url := urlRing[id]

				feels := words[fp:]

				invalid := false
				for _, feel := range feels {
					if len(feel) < 3 {
						reply("Feelings that are shorter than 3 characters aren't true feelings, %s.", getNick(message))
						invalid = true
						break
					}
					if len(feel) > maxFeelLen {
						reply("Feelings that are longer than %d characters aren't true feelings, %s.", maxFeelLen, getNick(message))
						invalid = true
						break
					}
					if feel[len(feel)-2:] != "ed" {
						reply("Feelings that don't end with 'ed' aren't true feelings, %s.", getNick(message))
						invalid = true
						break
					}
				}
				if invalid {
					break
				}

				for _, feel := range feels {
					feel = strings.ToLower(feel[:len(feel)-2])
					if pages[url].Feels[feel] == nil {
						pages[url].Feels[feel] = make(map[string]bool)
					}
					if pages[url].Feels[feel][getNick(message)] {
						continue
					}
					pages[url].Feels[feel][getNick(message)] = true

					oldTopNick := topFromFeelMap(mostFeelNicks, feel, 1)[0]
					oldTopPage := topFromFeelMap(mostFeelPages, feel, 1)[0]

					addMostFeel(mostFeelNicks, feel, pages[url].Poster, 1)
					addMostFeel(mostFeelPages, feel, url, 1)

					topNick := topFromFeelMap(mostFeelNicks, feel, 1)[0]
					topPage := topFromFeelMap(mostFeelPages, feel, 1)[0]
					if topNick != oldTopNick && topNick == pages[url].Poster {
						reply("Congratulations to %s, who is now pretty much the most %s person.", topNick, rootToGerund(feel))
					}
					if topPage != oldTopPage && topPage == url {
						reply("'%s' (%s) is now pretty much the most %s page.", pages[url].Title, url, rootToGerund(feel))
					}
				}

				reply("I feel you, %s.", getNick(message))
			case "!ring":
				if len(words) != 1 {
					break
				}
				for i, url := range urlRing {
					if url == "" {
						continue
					}
					reply("[%d] %s", i, url)
				}
			default:
				url := extractUrl(message.Trailing)
				if url != "" {
					url, err := normalizeUrl(url)
					if err != nil {
						l.Print(err)
						break
					}

					title, err := getTitle(url)
					if err != nil {
						l.Print(err)
						break
					}
					if title == "" {
						title = "[untitled]"
					}

					lastUrl++
					if lastUrl == len(urlRing) {
						lastUrl = 0
					}
					urlRing[lastUrl] = url
					if _, ok := pages[url]; !ok {
						pages[url] = Page{
							Url:    url,
							Title:  title,
							Poster: getNick(message),
							Date:   time.Now(),
							Feels:  make(map[string]map[string]bool),
						}
					}

					reply("[%d] %s", lastUrl, title)
				}
			}
		}

		pagesLock.Unlock()
	}
}
