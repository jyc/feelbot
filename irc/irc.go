package irc

import (
	"strings"
)

type Message struct {
	Prefix   string
	Command  string
	Params   []string
	Trailing string
}

func Parse(line string) *Message {
	parts := strings.Split(line, " ")
	message := new(Message)
	cp := 0

	if parts[0][0] == ':' {
		message.Prefix = parts[0][1:]
		cp++
	}

	message.Command = parts[cp]
	message.Params = make([]string, len(parts)-cp-1)
	np := 0

	for i := cp + 1; i < len(parts); i++ {
		if parts[i] == "" {
			continue
		}
		if parts[i][0] == ':' {
			trailingParts := make([]string, len(parts)-i)
			trailingParts[0] = parts[i][1:]
			for j := i + 1; j < len(parts); j++ {
				trailingParts[j-i] = parts[j]
			}
			message.Trailing = strings.Join(trailingParts, " ")
			break
		}
		message.Params[i-cp-1] = parts[i]
		np++
	}

	message.Params = message.Params[:np]

	return message
}

func Encode(message *Message) string {
	parts := make([]string, len(message.Params)+3)
	n := 0

	if message.Prefix != "" {
		parts[n] = ":" + message.Prefix
		n++
	}

	parts[n] = message.Command
	n++

	copy(parts[n:], message.Params)
	n += len(message.Params)

	if message.Trailing != "" {
		parts[n] = ":" + message.Trailing
		n++
	}

	return strings.Join(parts[:n], " ")
}
