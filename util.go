package main

import (
	"io"
	"net/http"
	"strings"
	"time"

	"code.google.com/p/go.net/html"

	"bitbucket.org/jyc/feelbot/irc"
)

const MaxReadSize = 100 << 10

var client http.Client

func init() {
	client.Timeout = 5 * time.Second
}

func getTitle(url string) (string, error) {
	resp, err := client.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	hb := make([]byte, MaxReadSize)
	read := 0
	for read < MaxReadSize {
		n, err := resp.Body.Read(hb[read:])
		if err == io.EOF {
			break
		}
		if err != nil && n == 0 {
			return "", err
		}

		read += n
	}
	hso := string(hb)
	hs := strings.ToLower(hso)
	a := strings.Index(hs, "<title")
	if a == -1 {
		return "", nil
	}
	b := strings.Index(hs[a+len("<title"):], ">")
	if b == -1 {
		return "", nil
	}
	c := strings.Index(hs[a+len("<title"):], "</title>")
	if c == -1 {
		return "", nil
	}

	title := hso[a+len("<title"):][b+1 : c]
	title = strings.Replace(title, "\n", "", -1)
	title = strings.Replace(title, "\r", "", -1)
	title = strings.Trim(title, " ")
	title = html.UnescapeString(title)
	return title, nil
}

func extractUrl(line string) string {
	a := -1
	if i := strings.Index(line, "http://"); i != -1 {
		a = i
	}
	if i := strings.Index(line, "https://"); i != -1 {
		a = i
	}
	if a == -1 {
		return ""
	}

	b := strings.Index(line[a:], " ")
	if b == -1 {
		b = len(line)
	} else {
		b = a + b
	}

	return line[a:b]
}

func getNick(message *irc.Message) string {
	n := strings.Index(message.Prefix, "!")
	if n == -1 {
		return ""
	}
	return message.Prefix[0:n]
}

func normalizeUrl(url string) (string, error) {
	resp, err := client.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	u := resp.Request.URL
	u.Scheme = "http"
	u.User = nil
	u.Fragment = ""

	return u.String(), nil
}
