# feelbot #

A mini link aggregator for IRC channels.

Not stable, needs refactoring.

# Developing #

Third party dependencies are stored in the `third_party` folder, which means before running any `go` command that uses the `GOPATH` you should source the included `activate` file in the shell you intend to use. This will prefix the `GOPATH` with the `third_party` folder. According to `go help gopath`:

> Go searches each directory listed in GOPATH to find source code,
> but new packages are always downloaded into the first directory
> in the list.

... which means that `go get` will correctly download new dependencies to the `third_party` folder, , `go build` will correctly use the `third_party` folder, etc. This lets us reliably build each commit with a compatible version of the dependency.

To "deactivate," simply run the command `deactivate`. I was sort of thinking of `virtualenv`, although I never really programmed much in Python.
